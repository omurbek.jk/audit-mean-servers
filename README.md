audit MEAN
============================
This stack will monitor MEAN and alert on things CloudCoreo developers think are violations of best practices


## Description
This repo is designed to work with CloudCoreo. It will monitor MEAN against best practices for you and send a report to the email address designated by the config.yaml AUDIT_MEAN_ALERT_RECIPIENT value

## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/audit-mean-servers/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_AWS_MEAN_ALERT_LIST`:
  * description: 
  * default: mongo-package-version, mongo-authenticated-connections-mongod0, mongo-key-rotation, web-node-package-version, web-connection-type, web-ssl-package-version, all-servers, mongo-servers, web-servers


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices
1. Alert
1. MEAN

## Categories
1. Audit



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/audit-mean-servers/master/images/diagram.png "diagram")


## Icon


