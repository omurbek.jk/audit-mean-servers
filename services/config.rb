# mongo cluster checks

# version of mongo is latest
# no non-authenticated connections are allowed
# key has been rotated in last 30 days

# web cluster checks

# version of node.js is fixed 7.8.0
# no http connections allowed - https only
# version of ssl is greater than some version that includes heartbleed fix

# for all - put in a failing rule to start

coreo_agent_selector_rule 'mean-mongo-server' do
  action :define
  timeout 15
  control 'mean-mongo-server' do
    describe command('mongod') do
      it { should exist }
    end
    describe file('/home/ec2-user/mongodb') do
      it { should exist }
    end
  end
end

coreo_agent_selector_rule 'mean-web-server' do
  action :define
  timeout 15
  control 'mean-web-server' do
    describe file('/home/ec2-user/mean/node_modules/karma/bin/karma') do
      it { should exist }
    end
  end
end

coreo_agent_selector_rule 'linux-server' do
  action :define
  timeout 15
  control 'linux-server' do
    describe command('uname') do
      its('stdout') { should eq "Linux\n" }
    end
  end
end

# mongo package is version 3.0.15

# mongodb-org-shell-3.0.15-1.amzn1.x86_64
# mongodb-org-server-3.0.15-1.amzn1.x86_64
# mongodb-org-mongos-3.0.15-1.amzn1.x86_64
# mongodb-org-3.0.15-1.amzn1.x86_64
# mongodb-org-tools-3.0.15-1.amzn1.x86_64

coreo_agent_audit_rule 'mongo-package-version' do
  action :define
  link 'http://kb.cloudcoreo.com/'
  display_name 'mongo-package-version'
  description 'mongo-package-version'
  category 'Security'
  suggested_action 'mongo-package-version'
  level 'low'
  selectors ['mean-mongo-server']
  timeout 15
  control 'mongo-package-version' do
    impact 1.0
    describe package('mongodb-org') do
      it { should be_installed }
      its('version') { should eq '3.0.15-1.amzn1' }
    end
  end
end

# the lines
#security:
#  keyFile: /tmp/mkey.js
# exist in one or more of the following files:
#/etc/mongod.conf
#/etc/mongod0.conf
#/etc/mongos.conf

coreo_agent_audit_rule 'mongo-authenticated-connections-mongod0' do
  action :define
  link 'http://kb.cloudcoreo.com/'
  display_name 'mongo-authenticated-connections-mongod0'
  description 'mongo-authenticated-connections-mongod0'
  category 'Security'
  suggested_action 'mongo-authenticated-connections-mongod0'
  level 'high'
  selectors ['mean-mongo-server']
  timeout 15
  control 'mongo-authenticated-connections-mongod0' do
    impact 1.0
    describe file('/tmp/mkey.js') do
      it { should exist }
    end
    only_if do
      file('/etc/mongod0.conf.sav').exist?
    end
    describe file('/etc/mongod0.conf') do
      its('content') { should match("keyFile") }
    end
  end
end

# the creation time of the file /tmp/mkey.js is within the last 30 days
# 86400 seconds in a day
# 2592000 in a 30 day month

coreo_agent_audit_rule 'mongo-key-rotation' do
  action :define
  link 'http://kb.cloudcoreo.com/'
  display_name 'mongo-key-rotation'
  description 'mongo-key-rotation'
  category 'Security'
  suggested_action 'mongo-key-rotation'
  level 'medium'
  selectors ['mean-mongo-server']
  timeout 15
  control 'mongo-key-rotation' do
    impact 1.0
    describe file('/tmp/mkey.js').mtime.to_i do
      it { should >= Time.now.to_i - 2592000 }
    end
  end
end

# the file /home/ec2-user/.nvm/versions/node/v6.9.1 exists

coreo_agent_audit_rule 'web-node-package-version' do
  action :define
  link 'http://kb.cloudcoreo.com/'
  display_name 'web-node-package-version'
  description 'web-node-package-version'
  category 'Security'
  suggested_action 'web-node-package-version'
  level 'low'
  selectors ['mean-web-server']
  timeout 15
  control 'web-node-package-version' do
    impact 1.0
    describe file('/home/ec2-user/.nvm/versions/node/v6.9.1') do
      it { should exist }
    end
  end
end

# file /home/ec2-user/mean/config/sslcerts/key.pem exists
# (this is a shallow test)...

coreo_agent_audit_rule 'web-connection-type' do
  action :define
  link 'http://kb.cloudcoreo.com/'
  display_name 'web-connection-type'
  description 'web-connection-type'
  category 'Security'
  suggested_action 'web-connection-type'
  level 'high'
  selectors ['mean-web-server']
  timeout 15
  control 'web-connection-type' do
    impact 1.0
    describe file('/home/ec2-user/mean/config/sslcerts/key.pem123') do
      it { should exist }
    end
  end
end

# package openssl-1.0.1k-15.99.amzn1.x86_64
# Affected users should upgrade to OpenSSL 1.0.1g

coreo_agent_audit_rule 'web-ssl-package-version' do
  action :define
  link 'http://kb.cloudcoreo.com/'
  display_name 'web-ssl-package-version'
  description 'web-ssl-package-version'
  category 'Security'
  suggested_action 'web-ssl-package-version'
  level 'high'
  selectors ['mean-web-server']
  timeout 15
  control 'web-ssl-package-version' do
    impact 1.0
    describe package('openssl') do
      it { should be_installed }
      its('version') { should eq '1.0.1k-15.99.amzn1' }
    end
  end
end

# all matching the selector will create a violation
#
coreo_agent_audit_rule 'all-servers' do
  action :define
  include_violations_in_count false
  category "Inventory"
  level "Informational"
  link 'http://kb.cloudcoreo.com/'
  display_name 'all-servers'
  description 'all-servers'
  suggested_action 'None'
  selectors ['linux-server']
  timeout 15
  control 'all-servers' do
    impact 1.0
    describe command('echo hello') do
      its('stdout') { should eq "world\n" }
    end
  end
end

# all matching the selector will create a violation
#
coreo_agent_audit_rule 'mongo-servers' do
  action :define
  include_violations_in_count false
  category "Inventory"
  level "Informational"
  link 'http://kb.cloudcoreo.com/'
  display_name 'mongo-servers'
  description 'mongo-servers'
  suggested_action 'None'
  selectors ['mean-mongo-server']
  timeout 15
  control 'mongo-servers' do
    impact 1.0
    describe command('echo hello') do
      its('stdout') { should eq "world\n" }
    end
  end
end

# all matching the selector will create a violation
#
coreo_agent_audit_rule 'web-servers' do
  action :define
  include_violations_in_count false
  category "Inventory"
  level "Informational"
  link 'http://kb.cloudcoreo.com/'
  display_name 'web-servers'
  description 'web-servers'
  suggested_action 'None'
  selectors ['mean-web-server']
  timeout 15
  control 'web-servers' do
    impact 1.0
    describe command('echo hello') do
      its('stdout') { should eq "world\n" }
    end
  end
end

coreo_agent_rule_runner 'audit-mean-servers' do
  action :run
  rules ${AUDIT_AWS_MEAN_ALERT_LIST}
  filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
